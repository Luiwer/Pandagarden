package pandagarden.pflanzenplaner;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/// Activity to edit plant items
public class EditPlantActivity extends AppCompatActivity {

    plantDataSource mDataSource;
    int mItemPosition;
    plantItem mItem;
    ImageView mIvPlant;
    EditText mEtName;
    Spinner mSpinnerLoc;
    EditText mEtPlant;
    EditText mEtFamily;
    EditText mEtCare;
    EditText mEtTasks;
    EditText mEtCalendar;
    EditText mEtNotes;
    Dialog mDialogIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_plant);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mDataSource = new plantDataSource(this);

        Intent intent = getIntent();
        mItemPosition = intent.getIntExtra("@string:intent_item_position",
                0);

        mDataSource.open();
        mItem = mDataSource.getPlantItem(mItemPosition);
        mDataSource.close();

        mIvPlant = findViewById(R.id.iv_plant_pic);
        mEtName = findViewById(R.id.et_name);
        mSpinnerLoc = findViewById(R.id.spinner_loc);
        mEtPlant = findViewById(R.id.et_plant_content);
        mEtFamily = findViewById(R.id.et_family_content);
        mEtCare = findViewById(R.id.et_care_content);
        mEtTasks = findViewById(R.id.et_tasks_content);
        mEtCalendar = findViewById(R.id.et_calendar_content);
        mEtNotes = findViewById(R.id.et_notes);

        // Set up Spinner
        mSpinnerLoc = findViewById(R.id.spinner_loc);
        ArrayAdapter<CharSequence> spin_adapter = ArrayAdapter
                .createFromResource(this, R.array.locations,
                        android.R.layout.simple_spinner_dropdown_item);
        spin_adapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        mSpinnerLoc.setAdapter(spin_adapter);

        setInitContent();

        // Save button
        FloatingActionButton fab_save = findViewById(R.id.fab_save);
        fab_save.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    saveContentToDb();
                    finish();
                }
            }
        );


        // Layout for icon choice
        // TODO outsource in own dialog description file as it is the same
        // as it is the same as in new_plant.java
        mDialogIcon = new Dialog(this);
        mDialogIcon.setContentView(R.layout.layout_icons);

        mIvPlant.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            mDialogIcon.show();
                                        }
                                    }
        );

        // Choice of icons
        ImageView iv_launcher = mDialogIcon.findViewById(R.id
                .iv_launcher);
        iv_launcher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIvPlant.setImageResource(android.R.drawable.sym_def_app_icon);
                mIvPlant.setTag(android.R.drawable.sym_def_app_icon);
                mDialogIcon.cancel();
            }
        });
        ImageView iv_lemon = mDialogIcon.findViewById(R.id
                .iv_lemon);
        iv_lemon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIvPlant.setImageResource(R.mipmap.ic_zitrone_foreground);
                mIvPlant.setTag(R.mipmap.ic_zitrone_foreground);
                mDialogIcon.cancel();
            }
        });
        ImageView iv_strawberry = mDialogIcon.findViewById(R.id
                .iv_strawberry);
        iv_strawberry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIvPlant.setImageResource(R.mipmap.ic_strawberry_foreground);
                mIvPlant.setTag(R.mipmap.ic_strawberry_foreground);
                mDialogIcon.cancel();
            }
        });
        ImageView iv_potato = mDialogIcon.findViewById(R.id
                .iv_potato);
        iv_potato.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIvPlant.setImageResource(R.mipmap.ic_potato_foreground);
                mIvPlant.setTag(R.mipmap.ic_potato_foreground);
                mDialogIcon.cancel();
            }
        });
        ImageView iv_salad = mDialogIcon.findViewById(R.id
                .iv_salad);
        iv_salad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIvPlant.setImageResource(R.drawable.ic_salad_foreground);
                mIvPlant.setTag(R.drawable.ic_salad_foreground);
                mDialogIcon.cancel();
            }
        });
        ImageView iv_tomato = mDialogIcon.findViewById(R.id
                .iv_tomato);
        iv_tomato.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIvPlant.setImageResource(R.mipmap.ic_tomato_foreground);
                mIvPlant.setTag(R.mipmap.ic_tomato_foreground);
                mDialogIcon.cancel();
            }
        });
    }

    /// Sets field contents as of detail view
    private void setInitContent()
    {
        int img_res = plantListAdapter.getResource(mItem.getImg());
        List<String> loc_array = Arrays.asList(
                getResources().getStringArray(R.array.locations));
        ArrayList<String> location = new ArrayList<String>(loc_array);
        mEtName.setText(mItem.getName());
        mSpinnerLoc.setSelection(location.indexOf(mItem.getLocation()));
        mEtPlant.setText(mItem.getPlant());
        mEtFamily.setText(mItem.getFamily());
        mEtCare.setText(mItem.getCare());
        mEtTasks.setText(mItem.getTasks());
        mEtCalendar.setText(mItem.getCalender());
        mEtNotes.setText(mItem.getNotes());
        mIvPlant.setImageResource(img_res);
        mIvPlant.setTag(img_res);
    }

    /// Saves changed data to the data base
    private void saveContentToDb()
    {
        Integer icon_id = (Integer) mIvPlant.getTag();
        if (icon_id == null)
            icon_id = android.R.drawable.sym_def_app_icon;
        mItem.setName(mEtName.getText().toString());
        mItem.setLocation(mSpinnerLoc.getSelectedItem().toString());
        mItem.setPlant(mEtPlant.getText().toString());
        mItem.setFamily(mEtFamily.getText().toString());
        mItem.setCare(mEtCare.getText().toString());
        mItem.setTasks(mEtTasks.getText().toString());
        mItem.setCalender(mEtCalendar.getText().toString());
        mItem.setNotes(mEtNotes.getText().toString());
        mItem.setImg(getResources().getResourceEntryName(icon_id));

        mDataSource.open();
        mDataSource.changeTableValues(mItem);
        mDataSource.close();
    }

    /// Creates a menu bar
    /// \param[in] menu   Menu bar to be created
    public boolean onCreateOptionsMenu(Menu menu)
    {
        return true;
    }

    /// Describes what happens if menu item has been clicked.
    /// \param[in] item    item to be selected.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
