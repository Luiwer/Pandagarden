package pandagarden.pflanzenplaner;

/// Item helper class containing information of each plant item
public class plantItem
{
    private String mName;
    private String mLocation;
    private String mPlant;
    private String mFamily;
    private String mCare;
    private String mTasks;
    private String mCalender;
    private String mNotes;
    private String mImg;
    private long mId;

    /// Creates new plant item
    /// \param[in] name     Name of item
    /// \param[in] location Location of the item
    /// \param[in] plant    Kind of plant of the item
    /// \param[in] family   Plant family of the item
    /// \param[in] care     Tipps about how to care the plant
    /// \param[in] tasks    Tasks for the plant
    /// \param[in] calender Calender items of the plant
    /// \param[in] notes    Notes about the plant
    /// \param[in] img      Icon image name
    /// \param[in] id       Sqlite id of the item
    public plantItem(String name, String location, String plant, String
            family, String care, String tasks, String calender, String notes,
                     String img, long id)
    {
        mName = name;
        mLocation = location;
        mPlant = plant;
        mFamily = family;
        mCare = care;
        mTasks = tasks;
        mCalender = calender;
        mNotes = notes;
        mImg = img;
        mId = id;
    }

    public void setName (String name)
    {
        mName = name;
    }

    public String getName ()
    {
        return mName;
    }

    public void setLocation (String location)
    {
        mLocation = location;
    }

    public String getLocation ()
    {
        return mLocation;
    }

    public void setPlant (String plant)
    {
        mPlant = plant;
    }

    public String getPlant ()
    {
        return mPlant;
    }

    public void setFamily (String family)
    {
        mFamily = family;
    }

    public String getFamily ()
    {
        return mFamily;
    }

    public void setCare(String care)
    {
        mCare = care;
    }

    public String getCare ()
    {
        return mCare;
    }

    public void setTasks (String tasks)
    {
        mTasks = tasks;
    }

    public String getTasks ()
    {
        return mTasks;
    }

    public void setCalender (String calendar)
    {
        mCalender = calendar;
    }

    public String getCalender ()
    {
        return mCalender;
    }

    public void setNotes (String notes)
    {
        mNotes = notes;
    }

    public String getNotes ()
    {
        return mNotes;
    }

    public void setImg(String img){ mImg= img; }

    public String getImg()
    {
        return mImg;
    }

    public void setId(long id)
    {
        mId = id;
    }

    public long getId()
    {
        return mId;
    }
}
