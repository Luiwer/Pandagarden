package pandagarden.pflanzenplaner;

import android.os.AsyncTask;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;
import java.net.URL;

/// Search view to search in database
public class search_db extends AppCompatActivity {

    EditText mEtSearch;
    TextView mTvPlantResult;
    TextView mTvFamilyLabel;
    TextView mTvFamilyResult;
    TextView mTvCareLabel;
    TextView mTvCareResult;
    TextView mTvCalendarLabel;
    TextView mTvCalendarResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_db);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mEtSearch = findViewById(R.id.et_search);
        mTvPlantResult = findViewById(R.id.tv_search_plant_result);
        mTvFamilyLabel = findViewById(R.id.tv_search_family_label);
        mTvFamilyResult = findViewById(R.id.tv_search_family_result);
        mTvCareLabel = findViewById(R.id.tv_search_care_label);
        mTvCareResult = findViewById(R.id.tv_search_care_result);
        mTvCalendarLabel = findViewById(R.id.tv_search_calendar_label);
        mTvCalendarResult = findViewById(R.id.tv_search_calendar_result);

        Button bSearch = findViewById(R.id.b_search);
        bSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                    new urlTask().execute(NetworkUtils.getUrl());
            }
        });
    }

    /// Creates a menu bar
    /// \param[in] menu   Menu bar to be created
    public boolean onCreateOptionsMenu(Menu menu)
    {
        return true;
    }

    /// Describes what happens if menu item has been clicked.
    /// \param[in] item    item to be selected.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /// Task for URL threading
    public class urlTask extends AsyncTask<URL, Void, String>
    {

        /// Load Url in background
        @Override
        protected String doInBackground(URL... urls) {
            URL url = urls[0];
            String results = null;
            try
            {
                results = NetworkUtils.getResponseFromUrl(url);
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            return results;
        }

        /// Shows result in search view
        /// \param[in] s  result from url
        @Override
        protected void onPostExecute(String s)
        {
            if (s != null && !s.equals(""))
            {
                JsonParser parser = new JsonParser();
                plantItem item = parser.getPlantItemFromSearch(s,mEtSearch
                        .getText().toString());
                if (-1 == item.getId())
                {
                    mTvPlantResult.setText(R.string.not_found);

                    // make every view invisible except plant_result
                    mTvPlantResult.setVisibility(View.VISIBLE);
                    mTvFamilyLabel.setVisibility(View.INVISIBLE);
                    mTvFamilyResult.setVisibility(View.INVISIBLE);
                    mTvCareLabel.setVisibility(View.INVISIBLE);
                    mTvCareResult.setVisibility(View.INVISIBLE);
                    mTvCalendarLabel.setVisibility(View.INVISIBLE);
                    mTvCalendarResult.setVisibility(View.INVISIBLE);
                }
                else
                {
                    mTvPlantResult.setText(item.getPlant());
                    mTvFamilyResult.setText(item.getFamily());
                    mTvCareResult.setText(item.getCare());
                    mTvCalendarResult.setText(item.getCalender());

                    // make views visible
                    mTvPlantResult.setVisibility(View.VISIBLE);
                    mTvFamilyLabel.setVisibility(View.VISIBLE);
                    mTvFamilyResult.setVisibility(View.VISIBLE);
                    mTvCareLabel.setVisibility(View.VISIBLE);
                    mTvCareResult.setVisibility(View.VISIBLE);
                    mTvCalendarLabel.setVisibility(View.VISIBLE);
                    mTvCalendarResult.setVisibility(View.VISIBLE);
                }
            }
        }
    }
}
