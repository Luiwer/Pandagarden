package pandagarden.pflanzenplaner;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import static pandagarden.pflanzenplaner.plantListAdapter.getResource;

public class DetailPlantActivity extends AppCompatActivity
{
    plantDataSource mDataSource;
    int mItemPosition;
    TextView mTvName;
    TextView mTvLoc;
    TextView mTvPlant;
    TextView mTvFamily;
    TextView mTvCare;
    TextView mTvTasks;
    TextView mTvCalendar;
    TextView mTvNotes;
    long mId;
    ImageView mIvIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_plant);

        mDataSource = new plantDataSource(this);

        mTvName = findViewById(R.id.tv_plant_name);
        mTvLoc = findViewById(R.id.tv_plant_loc);
        mTvPlant = findViewById(R.id.et_plant_content);
        mTvFamily = findViewById(R.id.et_family_content);
        mTvCare = findViewById(R.id.et_care_content);
        mTvTasks = findViewById(R.id.et_tasks_content);
        mTvCalendar = findViewById(R.id.et_calendar_content);
        mTvNotes = findViewById(R.id.tv_notes_content);
        mIvIcon = findViewById(R.id.iv_plant_pic);

        ImageView ivEdit = findViewById(R.id.iv_edit);
        ImageView ivDelete = findViewById(R.id
                .iv_delete);
        ImageView ivBack = findViewById(R
                .id.ivBackArrow);

        Intent intent = getIntent();
        mItemPosition = intent.getIntExtra
                ("@string:intent_item_position",0);

        setStartContent();

        // Start edit mode
        ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intentEdit = new Intent(getApplicationContext(),
                        EditPlantActivity.class);
                intentEdit.putExtra("@string:intent_item_position",
                        mItemPosition);
                startActivity(intentEdit);
            }
        });

        // Delete listener.
        ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                mDataSource.open();
                mDataSource.deletePlantItem(mId);
                mDataSource.close();
                finish();
            }
        });

        // Back listener.
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });
    }

    /// Describes what happend if the activity is restarted.
    @Override
    public void onResume()
    {
        setStartContent();
        super.onResume();
    }

    /// Sets content when create or resumed
    private void setStartContent()
    {
        mDataSource.open();
        final plantItem item = mDataSource.getPlantItem(mItemPosition);
        mDataSource.close();

        mId = item.getId();
        // Sets detail info
        mTvName.setText(item.getName());
        mTvLoc.setText(item.getLocation());
        mTvPlant.setText(item.getPlant());
        mTvFamily.setText(item.getFamily());
        mTvCare.setText(item.getCare());
        mTvTasks.setText(item.getTasks());
        mTvCalendar.setText(item.getCalender());
        mTvNotes.setText(item.getNotes());
        mIvIcon.setImageResource(getResource(item.getImg()));
    }
}
