package pandagarden.pflanzenplaner;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

/// Main activity
public class MainActivity extends AppCompatActivity {

    private plantListAdapter mPlantListAdapter;
    private RecyclerView mRVPlantList;

    /// Creates activity.
    /// Starts new plant activity when clicked on plus button.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRVPlantList = findViewById(R.id.rv_plant_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRVPlantList.setLayoutManager(layoutManager);
        mRVPlantList.setHasFixedSize(false);
        mPlantListAdapter = new plantListAdapter(this);
        mRVPlantList.setAdapter(mPlantListAdapter);

        // Button for adding new plants
        FloatingActionButton fab = findViewById(R.id.fab_add);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent_new_plant = new Intent(getBaseContext(), new_plant.class);
                startActivity(intent_new_plant);
            }
        });
    }

    /// Creates menu bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        getMenuInflater().inflate(R.menu.menu_optional, menu);
        return true;
    }

    /// Describes what happens if menu item has been clicked.
    /// \param[in] item    item to be selected.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.search)
        {
            Intent intent_search = new Intent(getBaseContext(), search_db.class);
            startActivity(intent_search);
        }

        return super.onOptionsItemSelected(item);
    }

    /// Describes what happend if the activity is restarted.
    @Override
    public void onResume()
    {
        mPlantListAdapter.checkChangedData();
        mPlantListAdapter.notifyDataSetChanged();
        super.onResume();
    }
}
