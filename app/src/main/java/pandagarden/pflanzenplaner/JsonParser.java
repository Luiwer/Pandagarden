package pandagarden.pflanzenplaner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/// Helper class for JSON files
public class JsonParser
{
    final String JSONSTRING = "Plants";
    final String JSONPLANT = "plant";
    final String JSONFAMILY = "family";
    final String JSONCARE = "care";
    final String JSONCALENDAR = "calendar";
    final String JSONDATE = "date";

    public JsonParser()
    {

    }
    /// Finds plant item named by the search_string
    /// \param[in] jsonstr  content of the json file
    /// \param[in] search_string  plant name which should be searched
    /// \returns plant item of the searched plant name. If id = -1 the plant
    /// was not found.
    public plantItem getPlantItemFromSearch(String jsonstr, String
            search_string)
    {
        plantItem item = new plantItem("","","","","","","","","",-1);
        if (jsonstr != null)
        {
            try
            {
                JSONObject jsonObject = new JSONObject(jsonstr);
                JSONArray jsonArray = jsonObject.getJSONArray(JSONSTRING);
                for (int i = 0; i < jsonArray.length(); ++i)
                {
                    JSONObject plant_i = jsonArray.getJSONObject(i);
                    String plant = plant_i.getString(JSONPLANT);
                    if (plant.equals(search_string))
                    {
                        JSONArray calendar_array = plant_i.getJSONArray
                                (JSONCALENDAR);
                        JSONObject calendar_obj = calendar_array
                                .getJSONObject(0);
                        item.setPlant(plant);
                        item.setFamily(plant_i.getString(JSONFAMILY));
                        item.setCare(plant_i.getString(JSONCARE));
                        item.setCalender(calendar_obj.getString(JSONDATE));
                        item.setId(1);
                        return item;
                    }
                }
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }
        return item;
    }

}
