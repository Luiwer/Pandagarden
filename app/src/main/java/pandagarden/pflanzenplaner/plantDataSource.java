package pandagarden.pflanzenplaner;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class plantDataSource
{
    private SQLiteDatabase mDatabase;
    private plantDbHelper mDbHelper;

    private String[] columns = {
            mDbHelper.COLUMN_ID,
            mDbHelper.COLUMN_NAME,
            mDbHelper.COLUMN_LOCATION,
            mDbHelper.COLUMN_PLANT,
            mDbHelper.COLUMN_FAMILY,
            mDbHelper.COLUMN_CARE,
            mDbHelper.COLUMN_TASKS,
            mDbHelper.COLUMN_CALENDAR,
            mDbHelper.COLUMN_NOTES,
            mDbHelper.COLUMN_IMG
    };

    /// Constructor
    public plantDataSource(Context context)
    {
        mDbHelper = new plantDbHelper(context);
    }

    /// Opens the database.
    public void open()
    {
        mDatabase = mDbHelper.getWritableDatabase();

    }

    /// Closes the database.
    public void close()
    {
        mDbHelper.close();
    }

    /// Adds an item in the database
    /// \param[in] name  Name of the plant
    /// \param[in] location Location of the plant
    /// \param[in] plant  Plant
    /// \param[in] family Plant family
    /// \param[in] care  Care tips for the plant
    /// \param[in] tasks  Tasks for the plant
    /// \param[in] calendar Calendar entries for the plant
    /// \param[in] notes  Notes for the plant item
    /// \param[in] img  Image string of the icon
    /// \returns plant item describing the data base
    public plantItem createPlantItem(String name, String location, String
            plant, String family, String care, String tasks, String calendar,
                                     String notes, String img)
    {
        ContentValues values = new ContentValues();
        values.put(mDbHelper.COLUMN_NAME, name);
        values.put(mDbHelper.COLUMN_LOCATION, location);
        values.put(mDbHelper.COLUMN_PLANT, plant);
        values.put(mDbHelper.COLUMN_FAMILY, family);
        values.put(mDbHelper.COLUMN_CARE, care);
        values.put(mDbHelper.COLUMN_TASKS, tasks);
        values.put(mDbHelper.COLUMN_CALENDAR, calendar);
        values.put(mDbHelper.COLUMN_NOTES, notes);
        values.put(mDbHelper.COLUMN_IMG, img);


        long insert_id = mDatabase.insert(mDbHelper.TABLE_PLANT_LIST, null,
                values);
        Cursor cursor = mDatabase.query(mDbHelper.TABLE_PLANT_LIST, columns,
                mDbHelper.COLUMN_ID + "=" + insert_id, null, null, null,
                null);
        cursor.moveToFirst();
        plantItem item = cursorToPlantItem(cursor);
        cursor.close();

        return item;
    }

    /// Returns plantItem object at index position in the database.
    /// \param[in] index position of the item in the database.
    /// \returns plantItem object at index position in the database.
    public plantItem getPlantItem(int index)
    {
        Cursor cursor = mDatabase.query(mDbHelper.TABLE_PLANT_LIST, columns,
                null, null, null, null, null);
        cursor.moveToPosition(index);
        plantItem item = null;
        if (!cursor.isAfterLast())
        {
            item = cursorToPlantItem(cursor);
        }
        cursor.close();

        return item;
    }

    /// \returns how many elements are in the table.
    public int getSizeOfTable ()
    {
        Cursor cursor = mDatabase.query(mDbHelper.TABLE_PLANT_LIST, columns,
                null, null, null, null, null);
        return cursor.getCount();
    }

    /// Deletes an item of the data base
    /// \param[in] id  item with id to be deleted.
    public void deletePlantItem(long id)
    {
        mDatabase.delete(mDbHelper.TABLE_PLANT_LIST,
                mDbHelper.COLUMN_ID + "=" + id,
                null);
    }

    /// Creates a plant item at a certain position of the data base
    /// \param[in] cursor   data base position
    /// \returns plant item with information of database at position
    private plantItem cursorToPlantItem(Cursor cursor)
    {
        String name = cursor.getString(cursor.getColumnIndex(mDbHelper
                .COLUMN_NAME));
        String location = cursor.getString(cursor.getColumnIndex(mDbHelper
                .COLUMN_LOCATION));
        String plant = cursor.getString(cursor.getColumnIndex(mDbHelper
                .COLUMN_PLANT));
        String family = cursor.getString(cursor.getColumnIndex(mDbHelper
                .COLUMN_FAMILY));
        String care = cursor.getString(cursor.getColumnIndex(mDbHelper
                .COLUMN_CARE));
        String tasks = cursor.getString(cursor.getColumnIndex(mDbHelper
                .COLUMN_TASKS));
        String calendar = cursor.getString(cursor.getColumnIndex(mDbHelper
                .COLUMN_CALENDAR));
        String notes = cursor.getString(cursor.getColumnIndex(mDbHelper
                .COLUMN_NOTES));
        String img = cursor.getString(cursor.getColumnIndex(mDbHelper
                .COLUMN_IMG));
        long id = cursor.getLong(cursor.getColumnIndex(mDbHelper.COLUMN_ID));

        return new plantItem(name, location, plant, family, care,
                tasks,calendar,notes, img, id);
    }

    /// Updates a database item which has the same id than plant_item.
    /// \param[in] plant_item  new values for the database.
    public void changeTableValues(plantItem plant_item)
    {
        ContentValues values = new ContentValues();
        values.put(mDbHelper.COLUMN_NAME, plant_item.getName());
        values.put(mDbHelper.COLUMN_LOCATION, plant_item.getLocation());
        values.put(mDbHelper.COLUMN_PLANT, plant_item.getPlant());
        values.put(mDbHelper.COLUMN_FAMILY, plant_item.getFamily());
        values.put(mDbHelper.COLUMN_CARE, plant_item.getCare());
        values.put(mDbHelper.COLUMN_TASKS, plant_item.getTasks());
        values.put(mDbHelper.COLUMN_CALENDAR, plant_item.getCalender());
        values.put(mDbHelper.COLUMN_NOTES, plant_item.getNotes());
        values.put(mDbHelper.COLUMN_IMG, plant_item.getImg());

        mDatabase.update(mDbHelper.TABLE_PLANT_LIST, values, mDbHelper
                        .COLUMN_ID + " = ?",new String[] { Integer.toString(
                (int)plant_item.getId()) });
    }
}
