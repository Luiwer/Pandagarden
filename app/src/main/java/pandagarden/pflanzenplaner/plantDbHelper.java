package pandagarden.pflanzenplaner;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static android.webkit.ConsoleMessage.MessageLevel.LOG;

public class plantDbHelper extends SQLiteOpenHelper
{
    public static final String DB_NAME = "plant_list.db";
    public static final int DB_VERSION = 3;
    public static final String TABLE_PLANT_LIST = "plant_list";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_LOCATION = "location";
    public static final String COLUMN_PLANT = "plant";
    public static final String COLUMN_FAMILY = "family";
    public static final String COLUMN_CARE = "care";
    public static final String COLUMN_TASKS = "tasks";
    public static final String COLUMN_CALENDAR = "calendar";
    public static final String COLUMN_NOTES = "notes";
    public static final String COLUMN_IMG ="img";

    public static final String SQL_CREATE =
            "CREATE TABLE " + TABLE_PLANT_LIST +
                    "(" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COLUMN_NAME + " TEXT NOT NULL, " +
                    COLUMN_LOCATION + " TEXT NOT NULL, " +
                    COLUMN_PLANT + " TEXT NOT NULL, " +
                    COLUMN_FAMILY + " TEXT NOT NULL, " +
                    COLUMN_CARE + " TEXT NOT NULL, " +
                    COLUMN_TASKS + " TEXT NOT NULL, " +
                    COLUMN_CALENDAR + " TEXT NOT NULL, " +
                    COLUMN_NOTES + " TEXT NOT NULL, " +
                    COLUMN_IMG + " TEXT NOT NULL);";

    public plantDbHelper(Context context)
    {
        super(context, DB_NAME, null, DB_VERSION);
    }

    // Will only be called if database does not exist
    @Override
    public void onCreate(SQLiteDatabase db)
    {
        try
        {
            db.execSQL(SQL_CREATE);
        }
        catch (Exception ex) {}

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLANT_LIST);
        onCreate(db);
    }
}
