package pandagarden.pflanzenplaner;

import android.net.Uri;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

/// Utils class for network issues
public class NetworkUtils
{
    final static String JSON_URL ="https://gitlab.com/Luiwer/Pandagarden/raw/master/plants.json";

    /// Creates the url to the JSON content
    public static URL getUrl()
    {
        Uri builtUri = Uri.parse(JSON_URL).buildUpon()
                .build();
        URL url = null;
        try{
            url = new URL(builtUri.toString());
        } catch (MalformedURLException e)
        {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return url;
    }

    /// Gets result from url
    /// \param[in] url
    /// \returns  content of url
    public static String getResponseFromUrl(URL url) throws IOException
    {

        HttpURLConnection urlConnection = (HttpURLConnection) url
                .openConnection();
        urlConnection.setRequestProperty("Content-Type", "application/json");
        urlConnection.setRequestProperty("Accept", "application/json");
        try
        {
            InputStream in = urlConnection.getInputStream();

            Scanner scanner = new Scanner(in);
            scanner.useDelimiter("\\A");

            boolean hasInput = scanner.hasNext();
            if (hasInput)
            {
                return scanner.next();
            }
            else
            {
                return null;
            }
        }finally {
            urlConnection.disconnect();
        }
    }
}
