package pandagarden.pflanzenplaner;

import android.app.Dialog;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.support.v7.widget.Toolbar;

/// Activity to add a new plant.
public class new_plant extends AppCompatActivity {

    plantDataSource mDataSource;
    Spinner mSpinnerLoc;
    Dialog mDialogIcon;
    Dialog mDialogSearch;
    ImageView mIvIcons;
    ImageView mIvSearch;

    /// Creates activity.
    /// Saves data in data source if save button is clicked
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_plant);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mDataSource = new plantDataSource(this);
        mIvIcons = findViewById(R.id.iv_plant_pic);
        mIvSearch = findViewById(R.id.iv_add_search);

        // Set up Spinner
        mSpinnerLoc = findViewById(R.id.spinner_loc);
        ArrayAdapter<CharSequence> spin_adapter = ArrayAdapter
                .createFromResource(this, R.array.locations,
                        android.R.layout.simple_spinner_dropdown_item);
        spin_adapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        mSpinnerLoc.setAdapter(spin_adapter);

        // Save button
        FloatingActionButton fab_save = findViewById(R.id.fab_save);
        fab_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText et_name = findViewById(R.id.et_name);
                EditText et_plant = findViewById(R.id.et_plant_content);
                EditText et_family = findViewById(R.id.et_family_content);
                EditText et_care = findViewById(R.id.et_care_content);
                EditText et_tasks = findViewById(R.id.et_tasks_content);
                EditText et_calendar = findViewById(R.id.et_calendar_content);
                EditText et_notes = findViewById(R.id.et_notes);

                Integer icon_id = (Integer) mIvIcons.getTag();
                if (icon_id == null)
                    icon_id = android.R.drawable.sym_def_app_icon;
                String icon = getResources().getResourceEntryName(icon_id);
                mDataSource.open();
                mDataSource.createPlantItem(et_name.getText().toString(),
                        mSpinnerLoc.getSelectedItem().toString(),
                        et_plant.getText().toString(),
                        et_family.getText().toString(),
                        et_care.getText().toString(),
                        et_tasks.getText().toString(),
                        et_calendar.getText().toString(),
                        et_notes.getText().toString(),
                        icon);
                mDataSource.close();
                finish();
            }
        });


        // Layout for search in database
        mDialogSearch = new Dialog(this);
        mDialogSearch.setContentView(R.layout.activity_add_search);
        Window window = mDialogSearch.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        mIvSearch.setOnClickListener(new View.OnClickListener() {
                                         @Override
                                         public void onClick(View v)
         {
             mDialogSearch.show();
         }
     }
        );

        // Layout for icon choice
        mDialogIcon = new Dialog(this);
        mDialogIcon.setContentView(R.layout.layout_icons);

        mIvIcons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialogIcon.show();
            }
        }
        );

        // Choice of icons
        ImageView iv_launcher = mDialogIcon.findViewById(R.id
                .iv_launcher);
        iv_launcher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIvIcons.setImageResource(android.R.drawable.sym_def_app_icon);
                mIvIcons.setTag(android.R.drawable.sym_def_app_icon);
                mDialogIcon.cancel();
            }
        });
        ImageView iv_lemon = mDialogIcon.findViewById(R.id
                .iv_lemon);
        iv_lemon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIvIcons.setImageResource(R.mipmap.ic_zitrone_foreground);
                mIvIcons.setTag(R.mipmap.ic_zitrone_foreground);
                mDialogIcon.cancel();
            }
        });
        ImageView iv_strawberry = mDialogIcon.findViewById(R.id
                .iv_strawberry);
        iv_strawberry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIvIcons.setImageResource(R.mipmap.ic_strawberry_foreground);
                mIvIcons.setTag(R.mipmap.ic_strawberry_foreground);
                mDialogIcon.cancel();
            }
        });
        ImageView iv_potato = mDialogIcon.findViewById(R.id
                .iv_potato);
        iv_potato.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIvIcons.setImageResource(R.mipmap.ic_potato_foreground);
                mIvIcons.setTag(R.mipmap.ic_potato_foreground);
                mDialogIcon.cancel();
            }
        });
        ImageView iv_salad = mDialogIcon.findViewById(R.id
                .iv_salad);
        iv_salad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIvIcons.setImageResource(R.drawable.ic_salad_foreground);
                mIvIcons.setTag(R.drawable.ic_salad_foreground);
                mDialogIcon.cancel();
            }
        });
        ImageView iv_tomato = mDialogIcon.findViewById(R.id
                .iv_tomato);
        iv_tomato.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIvIcons.setImageResource(R.mipmap.ic_tomato_foreground);
                mIvIcons.setTag(R.mipmap.ic_tomato_foreground);
                mDialogIcon.cancel();
            }
        });
    }

    /// Creates a menu bar
    /// \param[in] menu   Menu bar to be created
    public boolean onCreateOptionsMenu(Menu menu)
    {
        return true;
    }

    /// Describes what happens if menu item has been clicked.
    /// \param[in] item    item to be selected.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
