package pandagarden.pflanzenplaner;

import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/// Adapter for recycler view to list data source
public class plantListAdapter extends RecyclerView.Adapter<plantListAdapter
        .listViewholder>
{
    private int mNbListItems;
    private plantDataSource mDataSource;
    private Context mContext;

    /// Constructor
    public plantListAdapter(Context context)
    {
        mContext = context;
        mDataSource = new plantDataSource(context);
        mDataSource.open();
        mNbListItems = mDataSource.getSizeOfTable();
        mDataSource.close();
    }

    /// Create view holder for recycler view.
    /// \param[in] viewGroup  view of the context.
    /// \parma[in] viewType   type of view, not used.
    /// \returns view holder for recycler view.
    @Override
    public listViewholder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.layout_plant_items,
                viewGroup, false);
        listViewholder viewHolder = new listViewholder(view);
        return viewHolder;
    }

    /// Sets input of each list item
    /// \param[in] holder   list view holder, describing the layout of each
    /// item.
    /// \param[in] position  position of the list.
    @Override
    public void onBindViewHolder(listViewholder holder, int position)
    {
        mDataSource.open();

        plantItem item = mDataSource.getPlantItem(position);
        if (item != null)
        {
            holder.mPlantListTV.setText(item.getName());
            holder.mPlantLocTV.setText(item.getLocation());
            holder.mPlantPlantTV.setText(item.getPlant());
            String img = item.getImg();
            int resId = getResource(img);
            holder.mIconSmallIV.setImageResource(resId);
        }
        mDataSource.close();
    }

    /// \returns number of list items.
    @Override
    public int getItemCount()
    {

        return mNbListItems;
    }

    /// List view holder describing each individual item of the recycler view
    /// list
    class listViewholder extends RecyclerView.ViewHolder implements View.OnClickListener
    {

        TextView mPlantListTV;
        TextView mPlantLocTV;
        TextView mPlantPlantTV;
        ImageView mIconSmallIV;
        ConstraintLayout layout_plant_items;

        /// Constructor
        /// \param[in] itemView  view/ layout of the list item
        public listViewholder(View itemView)
        {
            super(itemView);
            layout_plant_items = itemView.findViewById(R.id.layout_plant_items);
            mPlantListTV = itemView.findViewById(R.id.tv_plant_name);
            mPlantLocTV = itemView.findViewById(R.id.tv_plant_loc);
            mPlantPlantTV = itemView.findViewById(R.id.tv_plant);
            mIconSmallIV = itemView.findViewById(R.id.iv_plant_pic_small);
            itemView.setOnClickListener(this);
        }

        /// Describes what happens if one item has clicked.
        /// Opens dialog of detailed view of the plant.
        /// \param[in] v   view of the list item.
        @Override
        public void onClick(View v) {
            Intent dialog_detail = new Intent(mContext,
                    DetailPlantActivity.class);
            dialog_detail.putExtra("@string:intent_item_position",
                    getAdapterPosition());
            mContext.startActivity(dialog_detail);
        }
    }

    /// Gets resource id of an image name.
    /// \param[in] img   name of the image.
    /// \returns resource id.
    public static int getResource(String img)
    {
        int res_id;
        switch (img)
        {
            case "ic_zitrone_foreground":
                res_id = R.mipmap.ic_zitrone_foreground;
                break;
            case "ic_strawberry_foreground":
                res_id = R.mipmap.ic_strawberry_foreground;
                break;
            case "ic_potato_foreground":
                res_id = R.mipmap.ic_potato_foreground;
                break;
            case "ic_salad_foreground":
                res_id = R.drawable.ic_salad_foreground;
                break;
            case "ic_tomato_foreground":
                res_id = R.mipmap.ic_tomato_foreground;
                break;
            case "sym_def_app_icon":
            default:
                res_id = android.R.drawable.sym_def_app_icon;
                break;
        }
        return res_id;
    }

    /// Adapt data set for plant list adapter in case the data source might
    /// have changed.
    public void checkChangedData()
    {
        mDataSource.open();
        mNbListItems = mDataSource.getSizeOfTable();
        mDataSource.close();
    }
}
